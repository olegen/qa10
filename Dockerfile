FROM golang:alpine
WORKDIR /usr/src/app
# pre-copy/cache go.mod for pre-downloading dependencies and only redownloading them in subsequent builds if they change
COPY api.go ./
RUN go mod init app && go mod tidy && go build -v -o /usr/local/bin/app ./...
EXPOSE 8082
CMD ["app"]
