# REBRAIN DevOps Task - QA08. Smoke test.

## About The Project
In this project was deployed go app with api, postgres as database, nginx as a reverse proxy in front.
We use docker image with go as a build env, and docker-compose as a deploy scenario.

For connection use `rebrain ssh key` and `ubuntu` user: `ssh ubuntu@oleg-do.devops.rebrain.srwx.net`.
Endpoint to smoke test : [http://oleg-do.devops.rebrain.srwx.net/qa/v1/test/smoke](http://oleg-do.devops.rebrain.srwx.net/qa/v1/test/smoke)

